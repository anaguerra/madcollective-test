<?php declare(strict_types=1);

namespace MadCollective\Interview\Tests\Domain\Models;

use MadCollective\Interview\Domain\Exception\VideoSourceException;
use MadCollective\Interview\Domain\Models\VideoSource;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class VideoSourceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testCreateVideo(): void
    {
        $videoSource = new VideoSource(Uuid::uuid4(), 'title', 'http://test.com', ['tag1', 'tag2']);
        $this->assertInstanceOf(VideoSource::class, $videoSource);
    }

    public function testCreateVideoWithInvalidDataThrowsExceptions(): void
    {
        $this->expectException(VideoSourceException::class);
        $this->expectExceptionMessage('Video title is empty');

        $videoSource = new VideoSource(Uuid::uuid4(), '', 'https://test.com', ['tag1']);
        $this->assertInstanceOf(VideoSource::class, $videoSource);
    }

    public function testToArray(): void
    {
        $uuid = Uuid::uuid4();
        $videoSource = new VideoSource($uuid, 'title', 'http://test.com', ['tag1', 'tag2']);
        $videoSourceArray = $videoSource->toArray();

        $this->assertIsArray($videoSourceArray);
        $this->assertEquals(
            [
                'uuid' => $uuid->toString(),
                'title' => 'title',
                'url' => 'http://test.com',
                'tags' => ['tag1', 'tag2'],
            ],
            $videoSourceArray
        );
    }
}
