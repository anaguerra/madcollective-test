<?php declare(strict_types=1);

namespace MadCollective\Interview\Tests\Infrastructure\Parsers;

use MadCollective\Interview\Infrastructure\Parsers\YamlReader;
use PHPUnit\Framework\TestCase;

class YamlReaderTest extends TestCase
{
    private YamlReader $parser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->parser = new YamlReader();
    }

    public function testParseToArray(): void
    {
        $array = $this->parser->toArray(__DIR__. '/../Samples/test.yaml');

        $this->assertIsArray($array);
//        $this->assertEquals([
//
//        ], $array);
    }
}
