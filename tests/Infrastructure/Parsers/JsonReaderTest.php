<?php declare(strict_types=1);

namespace MadCollective\Interview\Tests\Infrastructure\Parsers;

use MadCollective\Interview\Infrastructure\Parsers\JsonReader;
use PHPUnit\Framework\TestCase;

class JsonReaderTest extends TestCase
{
    private JsonReader $parser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->parser = new JsonReader();
    }

    public function testParseToArray(): void
    {
        $array = $this->parser->toArray(__DIR__. '/../Samples/test.json');

        $this->assertIsArray($array);
//        $this->assertEquals([
//
//        ], $array);
    }
}
