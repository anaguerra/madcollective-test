<?php declare(strict_types=1);

namespace Infrastructure\VideoSourceImporter;

namespace MadCollective\Interview\Tests\Infrastructure\VideoSourceImporter;

use Faker\Factory;
use Faker\Generator;
use MadCollective\Interview\Infrastructure\Parsers\JsonReader;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportDataCollection;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\VideoSourceGlorfImporter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class VideoSourceGlorfImporterTest extends TestCase
{
    private MockObject $jsonReader;
    private VideoSourceGlorfImporter $videoSourceGlorfImporter;
    private Generator $faker;
    private string $filename;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->jsonReader = $this->createMock(JsonReader::class);
        $this->videoSourceGlorfImporter = new VideoSourceGlorfImporter($this->jsonReader);
        $this->filename = 'test.yaml';
    }

    public function testImportFile(): void
    {
        $this->jsonReader->expects($this->once())
            ->method('toArray')
            ->with($this->filename)
            ->willReturn($this->getExampleRecord());

        $videoSourceImportDataCollection = $this->videoSourceGlorfImporter->import($this->filename);

        $this->assertInstanceOf(VideoSourceImportDataCollection::class, $videoSourceImportDataCollection);
        $this->assertCount(1, $videoSourceImportDataCollection);
    }


    private function getExampleRecord(): array
    {
        return [
            'videos' => [
                ['tags' => [$this->faker->word, $this->faker->word],
                'url' => $this->faker->url,
                'title' => $this->faker->title,
                ],
            ],
        ];
    }

    private function getExampleInvalidRecord(): array
    {
        return [
            'videos' => [
                ['etiquetas' =>  [$this->faker->word, $this->faker->word],
                'url' => $this->faker->url,
                'title' => $this->faker->title,
                ],
            ],
        ];
    }
}
