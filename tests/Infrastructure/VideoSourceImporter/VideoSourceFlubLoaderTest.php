<?php declare(strict_types=1);

namespace MadCollective\Interview\Tests\Infrastructure\VideoSourceImporter;

use Faker\Factory;
use MadCollective\Interview\Domain\Models\VideoSourceImportResult;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportData;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportDataCollection;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\VideoSourceFlubImporter;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\VideoSourceFlubLoader;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class VideoSourceFlubLoaderTest extends TestCase
{
    private MockObject $videoSourceFlubImporter;
    private VideoSourceFlubLoader $videoSourceFlubLoader;
    private \Faker\Generator $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->videoSourceFlubImporter = $this->createMock(VideoSourceFlubImporter::class);
        $this->videoSourceFlubLoader = new VideoSourceFlubLoader($this->videoSourceFlubImporter);
    }

    public function testLoader() : void
    {
        $absoluteFilePath = 'any-valid-file';
        $videoSourceImportDataCollection = new VideoSourceImportDataCollection([$this->generateData(), $this->generateData(), $this->generateData()]);
        $this->videoSourceFlubImporter->method('import')->willReturn($videoSourceImportDataCollection);

        $importResult = $this->videoSourceFlubLoader->loader($absoluteFilePath);

        $this->assertInstanceOf(VideoSourceImportResult::class, $importResult);
        $this->assertCount(3, $importResult->getVideoSourceCollection());
        $this->assertEmpty($importResult->getVideoSourceImportErrorCollection());
    }

    public function testLoaderWithBadDataInFile() : void
    {
        $absoluteFilePath = 'any-valid-file';
        $videoSourceImportDataCollection = new VideoSourceImportDataCollection($this->generateInvalidData());
        $this->videoSourceFlubImporter->method('import')->willReturn($videoSourceImportDataCollection);

        $importResult = $this->videoSourceFlubLoader->loader($absoluteFilePath);

        $this->assertInstanceOf(VideoSourceImportResult::class, $importResult);
        $this->assertEmpty($importResult->getVideoSourceCollection());
        $this->assertCount(2, $importResult->getVideoSourceImportErrorCollection());

        $this->assertEquals([
                [
                    'row' => 1,
                    'message' => 'Video title is empty',
                ],
                [
                    'row' => 2,
                    'message' => 'Invalid video url ""',
                ],
            ], $importResult->getVideoSourceImportErrorCollection()->jsonSerialize());
    }


    private function generateData(): VideoSourceImportData
    {
        return new VideoSourceImportData([
            'title' => $this->faker->sentence(3),
            'url' => $this->faker->url,
            'tags' => [$this->faker->word, $this->faker->word,],
        ]);
    }

    /**
     * @return VideoSourceImportData[]
     */
    private function generateInvalidData(): array
    {
        return [
            new VideoSourceImportData([
                'title' => '',
                'url' => 'https://test.com',
                'tags' => [$this->faker->word, $this->faker->word],
            ]),
            new VideoSourceImportData([
                'title' => 'valid title',
                'url' => '',
                'tags' => [$this->faker->word, $this->faker->word],
            ]),
        ];
    }
}
