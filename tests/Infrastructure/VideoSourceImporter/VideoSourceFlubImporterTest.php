<?php declare(strict_types=1);

namespace Infrastructure\VideoSourceImporter;

namespace MadCollective\Interview\Tests\Infrastructure\VideoSourceImporter;

use Faker\Factory;
use Faker\Generator;
use MadCollective\Interview\Domain\Exception\VideoSourceImportException;
use MadCollective\Interview\Infrastructure\Parsers\YamlReader;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportDataCollection;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\VideoSourceFlubImporter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class VideoSourceFlubImporterTest extends TestCase
{
    private MockObject $yamlReader;
    private VideoSourceFlubImporter $videoSourceFlubImporter;
    private Generator $faker;
    private string $filename;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
        $this->yamlReader = $this->createMock(YamlReader::class);
        $this->videoSourceFlubImporter = new VideoSourceFlubImporter($this->yamlReader);
        $this->filename = 'test.yaml';
    }

    public function testImportFile(): void
    {
        // TODO: ArrayIterator
        $this->yamlReader->expects($this->once())
            ->method('toArray')
            ->with($this->filename)
            ->willReturn([$this->getExampleRecord()]);

        $videoSourceImportDataCollection = $this->videoSourceFlubImporter->import($this->filename);
        $this->assertInstanceOf(VideoSourceImportDataCollection::class, $videoSourceImportDataCollection);
        $this->assertCount(1, $videoSourceImportDataCollection);
    }

    public function testImportFileWithInvalidKeyThrowsException(): void
    {
        $this->expectException(VideoSourceImportException::class);
        $this->expectExceptionMessage('Error in flub import: mandatory key "url" is not present');

        $recordWithInvalidKey = [
            'labels' => 'tag1, tag2',
            'name' => 'funny cats',
        ];
        $this->yamlReader->expects($this->once())
            ->method('toArray')
            ->with($this->filename)
            ->willReturn([$recordWithInvalidKey]);

        $this->videoSourceFlubImporter->import($this->filename);
    }

    public function testImportFileWithUnrecognizedFieldThrowsException(): void
    {
        $this->expectException(VideoSourceImportException::class);
        $this->expectExceptionMessage('Unexpected key/s "etiquetas"');

        $this->yamlReader->expects($this->once())
            ->method('toArray')
            ->with($this->filename)
            ->willReturn([$this->getExampleInvalidRecord()]);

        $this->videoSourceFlubImporter->import($this->filename);
    }

    private function getExampleRecord(): array
    {
        return [
            'labels' => sprintf('%s, %s', $this->faker->word, $this->faker->word),
            'name' => $this->faker->title,
            'url' => $this->faker->url,
        ];
    }

    private function getExampleInvalidRecord(): array
    {
        return [
            'etiquetas' => [$this->faker->word, $this->faker->word],
            'name' => $this->faker->title,
            'url' => $this->faker->url,
        ];
    }
}
