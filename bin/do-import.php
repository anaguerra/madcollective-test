<?php declare(strict_types=1);

use MadCollective\Interview\Domain\Models\VideoSourceTemplate;
use MadCollective\Interview\Infrastructure\Repository\InMemoryVideoSourceRepository;
use MadCollective\Interview\Infrastructure\Service\AppService;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\VideoSourceLoaderGeneric;

require_once __DIR__ . '/../src/Application/Config/Bootstrap.php';

$source = $argv[1];
$videoSourceTemplate = VideoSourceTemplate::fromValue($source);

$generic = AppService::getContainer()->get(VideoSourceLoaderGeneric::class);
$videoSourceImported = $generic->loader($videoSourceTemplate);

$repository = new InMemoryVideoSourceRepository();

foreach ($videoSourceImported->getVideoSourceCollection()->getItems() as $videoSource) {
    $repository->save($videoSource);

    print(sprintf(
        'importing: "%s"; Url: %s; Tags: %s',
        $videoSource->getTitle(),
        $videoSource->getUrl(),
        implode(',', $videoSource->getTags())
    )
    );
    print PHP_EOL;
}
