<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Exception;

class VideoSourceException extends \Exception implements DomainException
{
    public static function emptyTitle(): self
    {
        return new self('Video title is empty');
    }

    public static function invalidUrl(string $url): self
    {
        return new self(sprintf('Invalid video url "%s"', $url));
    }
}
