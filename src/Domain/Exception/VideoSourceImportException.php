<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Exception;

use MadCollective\Interview\Domain\Models\VideoSourceTemplate;

class VideoSourceImportException extends \Exception implements DomainException
{
    public static function invalidVideoSourceTemplate(VideoSourceTemplate $videoSourceTemplate): self
    {
        return new self(sprintf('VideoSourceTemplate "%s" is not implemented.', $videoSourceTemplate->value));
    }

    public static function mandatoryKeyNotPresent(VideoSourceTemplate $videoSourceTemplate, string $mandatoryKey): self
    {
        return new self(sprintf('Error in %s import: mandatory key "%s" is not present', $videoSourceTemplate->value, $mandatoryKey));
    }

    public static function invalidKeys(array $unexpectedKeys): self
    {
        return new self(sprintf('Unexpected key/s "%s"', implode('-', $unexpectedKeys)));
    }
}
