<?php

declare(strict_types=1);

namespace MadCollective\Interview\Domain\Exception;

interface DomainException extends \Throwable
{
}
