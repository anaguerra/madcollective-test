<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Exception;

class InvalidArgumentException extends \Exception implements DomainException
{
}
