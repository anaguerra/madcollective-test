<?php declare(strict_types=1);


namespace MadCollective\Interview\Domain\Models;

use MadCollective\Interview\Domain\Exception\VideoSourceException;
use Ramsey\Uuid\UuidInterface;

class VideoSource
{
    private UuidInterface $uuid;
    private string $title;
    private string $url;
    /** @var string[] */
    private array $tags;

    public function __construct(UuidInterface $uuid, string $title, string $url, array $tags)
    {
        $this->assertValidParams($title, $url);
        $this->uuid = $uuid;
        $this->title = $title;
        $this->url = $url;
        $this->tags = $tags;
    }

    public static function create(UuidInterface $uuid, string $title, string $url, array $tags): self
    {
        return new self($uuid, $title, $url, $tags);
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'title' => $this->title,
            'url' => $this->url,
            'tags' => $this->tags,
        ];
    }

    private function assertValidParams(string $title, string $url): void
    {
        if ('' === trim($title)) {
            throw VideoSourceException::emptyTitle();
        }

        if (false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw VideoSourceException::invalidUrl($url);
        }
    }
}
