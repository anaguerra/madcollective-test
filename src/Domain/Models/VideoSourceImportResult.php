<?php declare(strict_types=1);


namespace MadCollective\Interview\Domain\Models;

use MadCollective\Interview\Domain\Collections\VideoSourceCollection;
use MadCollective\Interview\Domain\Collections\VideoSourceImportErrorCollection;

class VideoSourceImportResult implements \JsonSerializable
{
    private VideoSourceCollection $videoSourceCollection;
    private VideoSourceImportErrorCollection $videoSourceImportErrorCollection;

    public function __construct(VideoSourceCollection $videoSourceCollection, VideoSourceImportErrorCollection $videoSourceImportErrorCollection)
    {
        $this->videoSourceCollection = $videoSourceCollection;
        $this->videoSourceImportErrorCollection = $videoSourceImportErrorCollection;
    }

    public static function create(VideoSourceCollection $videoSourceCollection, VideoSourceImportErrorCollection $videoSourceImportErrorCollection): self
    {
        $riderCollection = $videoSourceImportErrorCollection->hasError() ? new VideoSourceCollection() : $videoSourceCollection;

        return new self($riderCollection, $videoSourceImportErrorCollection);
    }

    public function getVideoSourceCollection(): VideoSourceCollection
    {
        return $this->videoSourceCollection;
    }

    public function getVideoSourceImportErrorCollection(): VideoSourceImportErrorCollection
    {
        return $this->videoSourceImportErrorCollection;
    }

    public function jsonSerialize() : array
    {
        return [
            'collection' => $this->getVideoSourceCollection()->getItems(),
            'errors' => $this->getVideoSourceImportErrorCollection()->getItems(),
        ];
    }
}
