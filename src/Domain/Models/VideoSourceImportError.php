<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Models;

class VideoSourceImportError implements \JsonSerializable
{
    private int $row;
    private string $message;

    public function __construct(int $row, string $message)
    {
        $this->row = $row;
        $this->message = $message;
    }

    public function getRow(): int
    {
        return $this->row;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function jsonSerialize(): array
    {
        return [
            'row' => $this->row,
            'message' => $this->message,
        ];
    }
}
