<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Models;

use BenSampo\Enum\Enum;

class VideoSourceTemplate extends Enum
{
    public const GLORF_SOURCE = 'glorf';
    public const FLUB_SOURCE = 'flub';

    public static function glorfSource(): self
    {
        return self::fromValue(self::GLORF_SOURCE);
    }

    public static function flubSource(): self
    {
        return self::fromValue(self::FLUB_SOURCE);
    }
}
