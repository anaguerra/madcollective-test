<?php declare(strict_types=1);

namespace MadCollective\Interview\Domain\Services;

//use Ramsey\Uuid\UuidInterface;

use MadCollective\Interview\Domain\Models\VideoSourceImportResult;
use MadCollective\Interview\Domain\Models\VideoSourceTemplate;

interface VideoSourceImporter
{
    // TODO: store imports (store y errores) (uuid)

    public function loader(
        VideoSourceTemplate $videoSourceTemplate,
        string $absoluteFilePath
    ): VideoSourceImportResult;
}
