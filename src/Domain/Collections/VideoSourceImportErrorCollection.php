<?php

declare(strict_types=1);

namespace MadCollective\Interview\Domain\Collections;

use ArrayIterator;
use MadCollective\Interview\Domain\Models\VideoSourceImportError;

class VideoSourceImportErrorCollection implements \IteratorAggregate, \Countable, \JsonSerializable
{
    /** @var VideoSourceImportError[] */
    private array $items = [];

    public function add(VideoSourceImportError $error): void
    {
        $this->items[] = $error;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return VideoSourceImportError[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function hasError(): bool
    {
        return $this->count() > 0;
    }

    public function jsonSerialize(): array
    {
        return array_map(
            fn (VideoSourceImportError $error) => $error->jsonSerialize(),
            $this->getItems()
        );
    }

    public function __toString(): string
    {
        $messages = array_map(fn (VideoSourceImportError $error) => sprintf('%s => %s)', $error->getRow(), $error->getMessage()), $this->items);
        return implode("\n", $messages);
    }
}
