<?php declare(strict_types=1);


namespace MadCollective\Interview\Domain\Collections;

use MadCollective\Interview\Domain\Models\VideoSource;

class VideoSourceCollection extends ObjectCollection
{
    /**
     * @return VideoSource[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public static function allowedObjectClass(): string
    {
        return VideoSource::class;
    }

    protected function itemAssertions($item): void
    {
    }

    public function jsonSerialize(): array
    {
        return array_map(
            fn (VideoSource $videoSource) => $videoSource->toArray(),
            $this->getItems()
        );
    }
}
