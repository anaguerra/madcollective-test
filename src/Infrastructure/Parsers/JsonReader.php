<?php declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\Parsers;

class JsonReader
{
    public function toArray(string $absoluteFilePath): array
    {
        /** @phpstan-ignore-next-line */
        return json_decode(file_get_contents($absoluteFilePath), true);
    }
}
