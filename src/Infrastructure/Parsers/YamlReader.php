<?php declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\Parsers;

class YamlReader
{
    public function toArray(string $absoluteFilePath): array
    {
        return yaml_parse_file($absoluteFilePath);
    }
}
