<?php declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\Repository;

use MadCollective\Interview\Domain\Models\VideoSource;
use MadCollective\Interview\Domain\Repository\VideoSourceRepository;

class InMemoryVideoSourceRepository implements VideoSourceRepository
{
    private array $items = [];

    public function __construct()
    {
    }

    public function save(VideoSource $videoSource): void
    {
        $this->items[] = $videoSource;
    }

    public function findAll(): array
    {
        return $this->items;
    }
}
