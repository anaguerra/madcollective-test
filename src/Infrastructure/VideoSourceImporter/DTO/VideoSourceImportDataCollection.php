<?php

declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class VideoSourceImportDataCollection extends DataTransferObjectCollection
{
    public function current(): VideoSourceImportData
    {
        return parent::current();
    }
}
