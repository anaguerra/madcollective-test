<?php

declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO;

use Spatie\DataTransferObject\DataTransferObject;

class VideoSourceImportData extends DataTransferObject
{
    public string $title;
    public string $url;
    public array $tags;
}
