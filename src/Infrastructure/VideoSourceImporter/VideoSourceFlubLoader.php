<?php declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\VideoSourceImporter;

use MadCollective\Interview\Domain\Collections\VideoSourceCollection;
use MadCollective\Interview\Domain\Collections\VideoSourceImportErrorCollection;
use MadCollective\Interview\Domain\Exception\VideoSourceException;
use MadCollective\Interview\Domain\Models\VideoSource;
use MadCollective\Interview\Domain\Models\VideoSourceImportError;
use MadCollective\Interview\Domain\Models\VideoSourceImportResult;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportData;
use Ramsey\Uuid\Uuid;
use Throwable;

class VideoSourceFlubLoader
{
    private VideoSourceFlubImporter $videoSourceFlubImporter;
    private string $defaultFilePath = __DIR__. '/../../../feed-exports/flub.yaml';

    public function __construct(VideoSourceFlubImporter $videoSourceFlubImporter)
    {
        $this->videoSourceFlubImporter = $videoSourceFlubImporter;
    }

    public function loader(?string $filePath): VideoSourceImportResult
    {
        $filePath = $filePath ?? $this->defaultFilePath;

        try {
            return $this->loadFile($filePath);
        } catch (Throwable $throwable) {
            $errorCollection = new VideoSourceImportErrorCollection();
            $errorCollection->add(new VideoSourceImportError(0, $throwable->getMessage()));
            return VideoSourceImportResult::create(new VideoSourceCollection(), $errorCollection);
        }
    }

    private function loadFile(string $absoluteFilePath): VideoSourceImportResult
    {
        $errorCollection = new VideoSourceImportErrorCollection();
        $videoSourceCollection = new VideoSourceCollection();

        $videoSourceImportDataCollection = $this->videoSourceFlubImporter->import($absoluteFilePath);

        foreach ($videoSourceImportDataCollection->items() as $index => $videoSourceImportData) {
            try {
                /** @var VideoSourceImportData $videoSourceImportData */
                $videoSource = VideoSource::create(
                    Uuid::uuid4(),
                    $videoSourceImportData->title,
                    $videoSourceImportData->url,
                    $videoSourceImportData->tags,
                );
                $videoSourceCollection->add($videoSource);
            } catch (VideoSourceException $videoSourceException) {
                $error = new VideoSourceImportError($index + 1, sprintf('%s', $videoSourceException->getMessage()));
                $errorCollection->add($error);
            }
        }

        return new VideoSourceImportResult($videoSourceCollection, $errorCollection);
    }
}
