<?php declare(strict_types=1);


namespace MadCollective\Interview\Infrastructure\VideoSourceImporter;

use MadCollective\Interview\Domain\Exception\VideoSourceImportException;
use MadCollective\Interview\Infrastructure\Parsers\JsonReader;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportData;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportDataCollection;

class VideoSourceGlorfImporter
{
    const FIELD_TITLE = 'title';
    const FIELD_URL = 'url';
    const FIELD_TAGS = 'tags';
    private JsonReader $jsonReader;

    public function __construct(JsonReader $jsonReader)
    {
        $this->jsonReader = $jsonReader;
    }


    // TODO: inject a Validator (as a parameter)
    public function import(string $filePath): VideoSourceImportDataCollection
    {
        $videoSourcesImportData = [];
        $array = $this->jsonReader->toArray($filePath)['videos'];

        foreach ($array as $record) {
            if ($this->isValidRecord($record)) {
                $videoSourcesImportData[] =
                    new VideoSourceImportData([
                        'title' => $record[self::FIELD_TITLE],
                        'url' => $record[self::FIELD_URL],
                        'tags' => array_key_exists(self::FIELD_TAGS, $record) ? $record[self::FIELD_TAGS] : [],
                    ]);
            }
        }
        return new VideoSourceImportDataCollection($videoSourcesImportData);
    }

    private function isValidRecord(array $record): bool
    {
        $unexpectedKeys = array_diff(array_keys($record), [self::FIELD_TITLE, self::FIELD_URL, self::FIELD_TAGS]);

        if (count($unexpectedKeys)) {
            throw VideoSourceImportException::invalidKeys($unexpectedKeys);
        }

        return true;
    }
}
