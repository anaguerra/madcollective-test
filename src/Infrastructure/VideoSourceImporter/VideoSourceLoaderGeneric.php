<?php
declare(strict_types=1);

namespace MadCollective\Interview\Infrastructure\VideoSourceImporter;

use MadCollective\Interview\Domain\Exception\VideoSourceImportException;
use MadCollective\Interview\Domain\Models\VideoSourceImportResult;
use MadCollective\Interview\Domain\Models\VideoSourceTemplate;
use MadCollective\Interview\Domain\Services\VideoSourceImporter;
use Ramsey\Uuid\UuidInterface;

class VideoSourceLoaderGeneric implements VideoSourceImporter
{
    public const NUMBER_OF_FIELDS = 3;
    private VideoSourceFlubLoader $videoSourceFlubLoader;
    private VideoSourceGlorfLoader $videoSourceGlorfLoader;


    public function __construct(
        VideoSourceFlubLoader $videoSourceFlubLoader,
        VideoSourceGlorfLoader $videoSourceGlorfLoader
    ) {
        $this->videoSourceFlubLoader = $videoSourceFlubLoader;
        $this->videoSourceGlorfLoader = $videoSourceGlorfLoader;
    }

    public function loader(
        VideoSourceTemplate $videoSourceTemplate,
        ?string $absoluteFilePath = null,
        ?UuidInterface $importedVideoSourceTemplateUuid = null
    ): VideoSourceImportResult {
        switch ($videoSourceTemplate->value) {
            case VideoSourceTemplate::FLUB_SOURCE:

                return $this->videoSourceFlubLoader->loader(
                    $absoluteFilePath,
                );

            case VideoSourceTemplate::GLORF_SOURCE:

                return $this->videoSourceGlorfLoader->loader(
                    $absoluteFilePath,
                );
        }

        throw VideoSourceImportException::invalidVideoSourceTemplate($videoSourceTemplate);
    }
}
