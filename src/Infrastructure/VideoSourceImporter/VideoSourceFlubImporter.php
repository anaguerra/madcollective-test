<?php declare(strict_types=1);


namespace MadCollective\Interview\Infrastructure\VideoSourceImporter;

use MadCollective\Interview\Domain\Exception\VideoSourceImportException;
use MadCollective\Interview\Domain\Models\VideoSourceTemplate;
use MadCollective\Interview\Infrastructure\Parsers\YamlReader;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportData;
use MadCollective\Interview\Infrastructure\VideoSourceImporter\DTO\VideoSourceImportDataCollection;

class VideoSourceFlubImporter
{
    const FIELD_TITLE = 'name';
    const FIELD_URL = 'url';
    const FIELD_TAGS = 'labels';
    private YamlReader $yamlReader;

    public function __construct(YamlReader $yamlReader)
    {
        $this->yamlReader = $yamlReader;
    }


    // TODO: inject a Validator (as a parameter)
    public function import(string $filePath): VideoSourceImportDataCollection
    {
        $videoSourcesImportData = [];
        $array = $this->yamlReader->toArray($filePath);

        foreach ($array as $record) {
            if ($this->isValidRecord($record)) {
                $videoSourcesImportData[] =
                    new VideoSourceImportData([
                        'title' => $record[self::FIELD_TITLE],
                        'url' => $record[self::FIELD_URL],
                        'tags' => array_key_exists(self::FIELD_TAGS, $record) ? explode(',', $record[self::FIELD_TAGS]) : [],
                    ]);
            }
        }
        return new VideoSourceImportDataCollection($videoSourcesImportData);
    }

    private function isValidRecord(array $record): bool
    {
        if (false === array_key_exists(self::FIELD_TITLE, $record)) {
            throw VideoSourceImportException::mandatoryKeyNotPresent(VideoSourceTemplate::flubSource(), self::FIELD_TITLE);
        }

        if (false === array_key_exists(self::FIELD_URL, $record)
        ) {
            throw VideoSourceImportException::mandatoryKeyNotPresent(VideoSourceTemplate::flubSource(), self::FIELD_URL);
        }

        $unexpectedKeys = array_diff(array_keys($record), [self::FIELD_TITLE, self::FIELD_URL, self::FIELD_TAGS]);

        if (count($unexpectedKeys)) {
            throw VideoSourceImportException::invalidKeys($unexpectedKeys);
        }

        return true;
    }
}
