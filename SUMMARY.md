## Video Share

**Installation and execution**

1) `composer install`
2) Run the tests with `composer phpunit`
3) Run the application with `php bin/do-import.php flub` or `php bin/do-import.php glorf`


**Summary**

- The code is located in `src` and the tests are in `tests`
- I've focused on design and try to isolate providers and importation, so when another provider is added only his classes
  must be developed and the generic loader only would add some new lines.
- There are several tests but I've not done all that should be required.
- Some improvements could be done but for the purposes of the test I think that is enough so you can see how I develop.
- It's not my first time doing tests.
- If I had more time I would develop a validation system that could be injected in all the importer classes to validate the file data.
- Also I would develop the persistence layer (with the help of a framework)
- Also I would do a more elaborated Domain entity "VideoSource", with some Value Objects.
- I've used the package `php-di` for helping me the dependency injection


* * * 
## Delivery

Please create a git patch (https://devconnected.com/how-to-create-and-apply-git-patch-files/) and email your completed test to me.
